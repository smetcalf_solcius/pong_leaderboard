#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>

const int trigger1 = 14; // D5
const int echo1 = 12; // D6
const int trigger2 = 5; // D1
const int echo2 = 4; // D2

const int sleepTimeS = 3;

const char* ssid = "";
const char* password = "";
const char* host = "172.16.0.10:3000";

void setup() {
  Serial.begin (115200);
  Serial.println("");
  pinMode(trigger1, OUTPUT);
  pinMode(echo1, INPUT);
  pinMode(trigger2, OUTPUT);
  pinMode(echo2, INPUT);

  WiFi.hostname("Pong_Sensor");
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }
  Serial.println("connected");
  
  sendStatusToServer(personDetected());
  
//  if (personDetected()) {
//    //Serial.println("Person Detected");
//  }
//  else {
//    //Serial.println("Nothing");
//  }
  delay(100);
  ESP.deepSleep(sleepTimeS *1000000);
}

void loop() {

}

bool personDetected() {
  int loops = 10;
  int hitRange = 70;
  int hitsToTrigger = 2;
  
  int hits = 0;
  for (int i = 0; i < loops; i++) {
    if (getDistance(trigger1,echo1) < hitRange){
      hits++;
    }
    delay(50);
  }

  for (int i = 0; i < loops; i++) {
    if (getDistance(trigger2,echo2) < hitRange){
      hits++;
    }
    delay(50);
  }
  
  if (hits >= hitsToTrigger) {
    return true;
  }
  else {
    return false;
  }
}

double getDistance(int triggerPin, int echoPin) {
  digitalWrite(triggerPin, LOW);  // Get Start
  delayMicroseconds(2); // stable the line 
  digitalWrite(triggerPin, HIGH); // sending 10 us pulse
  delayMicroseconds(10); // delay
  digitalWrite(triggerPin, LOW); // after sending pulse wating to receive signals
  double duration = pulseIn(echoPin, HIGH); // calculating time 
  Serial.println((duration/2) / 29.1);
  return (duration/2) / 29.1; // single path 
}

void sendStatusToServer(bool tableStatus) {
  String uri;
  if (tableStatus) {
    uri = "/sensorData?occupied=true"; 
  }
  else {
    uri = "/sensorData?occupied=false";
  }

  Serial.print("sending message: ");
  Serial.println("http://"+(String)host+uri);
  HTTPClient http;
  http.begin("http://"+(String)host+uri);
  if (http.GET()) {
    http.end();
  }
}
