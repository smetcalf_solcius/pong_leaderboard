// Declare main application
var app = angular.module('pongApp',[]);

// Main Controller
app.controller('mainController', function(dataQuery) {
    var self = this;
    self.players = [];

    self.detailMode = 'Stats';
    self.playerClickFunctions = {
        'Stats' : function (name) {
            if (name != self.selectedPlayer1.name) {
                self.selectedPlayer1 = getPlayerFromArray(name);
            }
            else {
                self.selectedPlayer1 = {};
            }
        },
        'Games' : function (name) {
            if (name != self.selectedPlayer1.name) {
                self.selectedPlayer1 = getPlayerFromArray(name);
            }
            else {
                self.selectedPlayer1 = {};
            }
        }
    }

    self.selectedPlayer1 = {};

    self.winner;
    self.loser;

    self.postingError = '';
    self.tableStatus = '';

    // Exposed functions
    self.postGame = function () {
        dataQuery.postGame(self.winner, self.loser)
            .then(response => {
                if (response.data.error) {
                    self.postingError = response.data.error;
                }
                else {
                    self.players = generatePlayerArray(response.data);
                    self.winner = null;
                    self.loser = null;
                    self.postingError = '';
                    if (self.selectedPlayer1.name != undefined) {
                        self.selectedPlayer1 = getPlayerFromArray(self.selectedPlayer1.name);
                    }
                }
            });
    }    
    self.updateDetailMode = function (mode) {
        self.detailMode = mode;
        //self.selectedPlayer1 = {};
    }
    self.showArrow = function (change) {
        if (change > 0) {
            return 'green_arrow.PNG';
        }
        else {
            return 'red_arrow.PNG';
        }
    }

    // Private functions
    function generatePlayerArray (data) {
        var array = [];
        angular.forEach(data, function (val, key) {
            array.push(val);
        });
        return array;
    }
    function getPlayerFromArray (name) {
        for (var i = 0; i < self.players.length; i++) {
            if (name == self.players[i].name) {
                return self.players[i]
            }
        }
        return null;
    }

    // Run on start
    dataQuery.getPlayers()
        .then(response => {
            self.players = generatePlayerArray(response.data);
        });
    dataQuery.getTableStatus()
        .then(response => {
            self.tableStatus = response.data;
        });

    // Requery for table status every 30 seconds
    setInterval(function() {
        dataQuery.getTableStatus()
        .then(response => {
            self.tableStatus = response.data;
        });
    }, 30000);
});

// Factory to communicate with server
app.factory('dataQuery', function($http) {
    var factory = {};

    factory.getPlayers = function () {
        return $http.get('/getPlayers');
    }

    factory.getTableStatus = function () {
        return $http.get('/getTableStatus');
    }

    factory.postGame = function (winner, loser) {
        var params = '?winner=' + winner + '&loser=' + loser;
        return $http.get('/postGame' + params);
    }

    return factory;
});